package pkg

type Canvas struct {
	Colors [][]Color
	Width  int
	Height int
}
