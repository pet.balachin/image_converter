package internal

import (
	"bufio"
	"image_converter/pkg"
	"math"
	"strconv"
	"strings"
)

type PpmReader struct {
}

func (pr *PpmReader) Read(scan bufio.Scanner) pkg.Canvas {
	str := pr.getFilteredImageString(scan)
	canvas := pr.DrawCanvas(str)
	return canvas
}

func (pr *PpmReader) DrawCanvas(file string) pkg.Canvas {
	ppmValues := strings.Split(file, " ")

	width, _ := strconv.Atoi(ppmValues[1])
	height, _ := strconv.Atoi(ppmValues[2])
	maxColor, _ := strconv.Atoi(ppmValues[3])
	baseColor := 255

	canvas := pkg.Canvas{
		Colors: make([][]pkg.Color, height),
		Width:  width,
		Height: height,
	}
	for i := range canvas.Colors {
		canvas.Colors[i] = make([]pkg.Color, width)
	}

	for i := height - 1; i >= 0; i-- {
		for j := 0; j < width; j++ {
			index := i*(width*3) + (j * 3) + 3
			r, _ := strconv.Atoi(ppmValues[index])
			g, _ := strconv.Atoi(ppmValues[index+1])
			b, _ := strconv.Atoi(ppmValues[index+2])
			canvas.Colors[i][j] = pkg.Color{
				R: normalizeColor(r, maxColor, baseColor),
				G: normalizeColor(g, maxColor, baseColor),
				B: normalizeColor(b, maxColor, baseColor),
			}
		}
	}

	return canvas

}

func (pr *PpmReader) getFilteredImageString(scan bufio.Scanner) string {
	strs := make([]string, 0)
	for scan.Scan() {
		text := scan.Text()
		if !strings.ContainsRune(text, '#') {
			strs = append(strs, scan.Text())
		}
	}
	return strings.Join(strs, " ")
}

func normalizeColor(color, maxColor, baseColor int) byte {
	return (byte)(math.Floor(float64(color) / float64(maxColor*baseColor)))
}
