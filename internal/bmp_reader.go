package internal

import (
	"bufio"
	"image_converter/pkg"
	"os"
)

const HeaderSize = 138

type BmpReader struct {
}

func (br *BmpReader) Read(file string) pkg.Canvas {
	f, _ := os.Open(file)
	reader := *bufio.NewReader(f)
	width, height := getBmpDimensions(f)

	canvas := br.DrawCanvas(reader, (int)(width), (int)(height))
	return canvas
}

func (br *BmpReader) DrawCanvas(reader bufio.Reader, width, height int) pkg.Canvas {
	canvas := pkg.Canvas{
		Colors: make([][]pkg.Color, height),
		Width:  width,
		Height: height,
	}
	for i := range canvas.Colors {
		canvas.Colors[i] = make([]pkg.Color, width)
	}
	for i := height - 1; i >= 0; i-- {
		for j := 0; j < width; j++ {
			r, _ := reader.ReadByte()
			g, _ := reader.ReadByte()
			b, _ := reader.ReadByte()

			canvas.Colors[i][j] = pkg.Color{
				R: r,
				G: g,
				B: b,
			}
		}
	}
	return canvas
}

func getBmpDimensions(file *os.File) (width int, height int) {
	bytes := make([]byte, 8)
	file.ReadAt(bytes, 18)
	width = int(bytes[3])<<24 | int(bytes[2])<<16 | int(bytes[1])<<8 | int(bytes[0])
	height = int(bytes[7])<<24 | int(bytes[6])<<16 | int(bytes[5])<<8 | int(bytes[4])
	return
}
