package main

import (
	"flag"
	"image_converter/internal"
	"strings"
)

const (
	DEFAULT_SOURCE = "all_gray.bmp"
	DEFAULT_OUTPUT = "output.ppm"
)

type Config struct {
	SourceFile,
	OutputFile,
	SourceFormat,
	OutputFormat string
}

func (cfg *Config) setFromFlags() {
	flag.StringVar(&cfg.SourceFile, "source", DEFAULT_SOURCE, "Path of source file to convert")
	flag.StringVar(&cfg.OutputFile, "output", DEFAULT_OUTPUT, "Path of file to save result")
	flag.Parse()
	cfg.SourceFormat = strings.Split(cfg.SourceFile, ".")[1]
	cfg.OutputFormat = strings.Split(cfg.OutputFile, ".")[1]
}

func main() {
	cfg := Config{}
	cfg.setFromFlags()
	br := internal.BmpReader{}
	c := br.Read(cfg.SourceFile)
	pw := internal.PpmWriter{}
	pw.Write(c, cfg.OutputFile)
}
